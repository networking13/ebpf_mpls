#define MPLS_LS_LABEL_SHIFT 12
#define MPLS_LS_TC_SHIFT 9
#define MPLS_LS_S_SHIFT 8
#define MPLS_LS_TTL_SHIFT 0

struct mpls_hdr {
	unsigned int entry;
};

static inline struct mpls_hdr mpls_encode(unsigned int label, unsigned int ttl,
													 unsigned int tc, bool bos)
{
	struct mpls_hdr result;
	result.entry =
		//we need to convert from CPU endian to network endian
		bpf_htonl((label << MPLS_LS_LABEL_SHIFT) |
			    (tc << MPLS_LS_TC_SHIFT) |
			    (bos ? (1 << MPLS_LS_S_SHIFT) : 0) |
			    (ttl << MPLS_LS_TTL_SHIFT));
	return result;
}
