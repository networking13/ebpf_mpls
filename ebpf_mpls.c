#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/pkt_cls.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>
#include <stdbool.h>
#include "mpls.h"
#define MPLS_STATIC_LABEL 88

#define bpf_debug_printk(fmt, ...)                               \
  ({                                                             \
      char ____fmt[] = fmt;                                      \
      bpf_trace_printk(____fmt, sizeof(____fmt), ##__VA_ARGS__); \
  })

#define DEBUG(x, ...) bpf_debug_printk(x, ##__VA_ARGS__)
#define DEBUG_ENCAP(id, x, ...) DEBUG("[encap][%u]" x, id, ##__VA_ARGS__)
#define REQUEST_ID() bpf_get_prandom_u32()

SEC("encap")
int encap_prog(struct __sk_buff *skb) {
    unsigned long long request_id = REQUEST_ID();
    void *data = (void *)(long)skb->data;
    void *data_end = (void *)(long)skb->data_end;
    struct ethhdr *eth = data;

    //check if we will not try to access the memory range over the packet size
    if ((void *)(eth + 1) > data_end){
        DEBUG_ENCAP(request_id, "socket buffer struct was malformed.\n");
        return TC_ACT_OK;
    }

    //check if the packet is of type IPV4
    if (eth->h_proto != bpf_htons(ETH_P_IP)){ 
        DEBUG_ENCAP(request_id, " ethernet is not wrapping IP packet: 0x%x\n",
                bpf_ntohs(eth->h_proto));
        return TC_ACT_OK;
    }

    eth->h_proto = bpf_htons(ETH_P_MPLS_UC);
        
    //padding to add space
    int padlen = sizeof(struct mpls_hdr);

    //add space for mpls header
    int result = bpf_skb_adjust_room(skb, padlen, BPF_ADJ_ROOM_MAC, 0);
    if (result){
        DEBUG_ENCAP(request_id, "error calling skb adjust room.\n");
        return TC_ACT_SHOT;
    }
    
    DEBUG_ENCAP(request_id, "about to store bytes of MPLS label: 0x%x\n",
              MPLS_STATIC_LABEL);
    
    struct mpls_hdr mpls = mpls_encode(MPLS_STATIC_LABEL, 123, 0, true);

    unsigned long offset = sizeof(struct ethhdr);
    result = bpf_skb_store_bytes(skb, (int)offset, &mpls, sizeof(struct mpls_hdr),
                            BPF_F_RECOMPUTE_CSUM);

    if (result){
        DEBUG_ENCAP(request_id, "error calling skb store bytes.\n");
        return TC_ACT_SHOT;
    }

    DEBUG_ENCAP(request_id, "finished mpls encap.\n");
    return TC_ACT_OK;
}

char _license[] SEC("license") = "GPL";
